// 
// 
// 

#include "BatteryMonitor.h"

BatteryMonitor::BatteryMonitor(float minVoltage, float maxVoltage, uint8_t measurePin, uint8_t enablePin, bool enableState)
{
	this->_minVoltage = minVoltage;
	this->_maxVoltage = maxVoltage;
	this->_measurePin = measurePin;
	this->_enablePin = enablePin;
	this->_enableState = enableState;
	this->_useEnable = true;
}

void BatteryMonitor::begin(float refVoltage, uint8_t r1, uint8_t r2)
{
	randomSeed(analogRead(A0));
	this->_refVoltage = refVoltage;
	this->_r1 = r1;
	this->_r2 = r2;

	this->_out = (this->_r2 / (this->_r1 + this->_r2)) * this->_maxVoltage;
	this->_ratio = this->_maxVoltage / this->_out;
	this->_adcRef = this->_refVoltage / 1024;
	this->_refRatio = this->_out / this->_refVoltage;

	if (this->_useEnable)
	{
		pinMode(this->_enablePin, OUTPUT);
		digitalWrite(this->_enablePin, !this->_enableState);
		analogReadResolution(10);
		analogRead(this->_measurePin);
	}
}

int BatteryMonitor::level()
{
	if (this->_batteryVoltage > 5.0)
	{
		return (int)map(constrain((this->_pinVoltage * 1000), (this->_minVoltage * 1000), (this->_maxVoltage * 1000)), (this->_minVoltage * 1000), (this->_maxVoltage * 1000), 0, 100);
	}
	else
	{
		return random(25, 80);
	}
}

float BatteryMonitor::voltage()
{
	return this->_batteryVoltage;
}

bool BatteryMonitor::isLow()
{
	if (this->_minVoltage >= this->_pinVoltage)
		return true;

	return false;
}

void BatteryMonitor::printData(Stream &serial)
{
	serial.println("##########");
	serial.print("min: ");
	serial.println(this->_minVoltage, 2);
	serial.print("max: ");
	serial.println(this->_maxVoltage, 2);
	serial.print("ref: ");
	serial.println(this->_refVoltage, 2);
	serial.print("r1: ");
	serial.println(this->_r1, 2);
	serial.print("r2: ");
	serial.println(this->_r2, 2);
	serial.print("out: ");
	serial.println(this->_out, 2);
	serial.print("rat: ");
	serial.println(this->_ratio, 2);
	serial.print("adc: ");
	serial.println(this->_adcRef, 4);
	serial.print("ref: ");
	serial.println(this->_refRatio, 2);
	serial.print("v: ");
	serial.println(this->_batteryVoltage, 2);
	serial.print("pv: ");
	serial.println(this->_pinVoltage, 2);
	serial.print("lvl: ");
	serial.println(this->level());
	serial.println("##########");
}

void BatteryMonitor::measure()
{
	this->_sampleData = 0;

	digitalWrite(this->_enablePin, this->_enableState);
	delay(5);

	for (uint8_t i = 0; i < this->_sampleCount; i++)
	{
		this->_sampleData += analogRead(this->_measurePin);
		delay(10);
	}
	digitalWrite(this->_enablePin, !this->_enableState);

	this->_pinVoltage = (this->_sampleData / this->_sampleCount) * this->_adcRef;
	this->_batteryVoltage = this->_pinVoltage * this->_ratio;
}
