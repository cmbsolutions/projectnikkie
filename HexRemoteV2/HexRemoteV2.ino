/*
    Name:       HexRemoteV2.ino
    Created:	8-9-2018 21:10:52
    Author:     GOLLEM\Maurice
*/

#include "config.h"
#include "BatteryMonitor.h"

#include <Nextion.h>
#include <NextionPage.h>
#include <NextionButton.h>
#include <NextionVariableNumeric.h>
#include <NextionVariableString.h>

#include <XBee.h>
#include <Bounce2.h>
#include <elapsedMillis.h>

static const boolean g_afAnalogInvert[] = { true, true, true, true, true, true };
static const word g_awAnalogMins[] = { 0 * 8, 0 * 8, 0 * 8, 0 * 8, 0 * 8, 0 * 8 };
static const word g_awAnalogMaxs[] = { 1023 * 8, 1023 * 8, 1023 * 8, 1023 * 8, 1023 * 8, 1023 * 8 };

char shutdownPage[] = "page 7";


uint8_t g_fLimitRange = false;
word g_awRawAnalog[8][NUMANALOGS];		/* Keep 8 raw values for each analog input */
word g_awRawBatAnalog[8];
word g_awRawSums[NUMANALOGS];			/* sum of the raw values */
word g_awAnalogMids[NUMANALOGS];		/* This is our observed center points at init time */
byte g_iRaw;							/* which raw value are we going to read into */

word g_bButtons;
word g_abJoyVals[NUMANALOGS];			/* Joystick values */

Nextion nex(NEXTION_PORT);
Bounce bButtons[] = { Bounce(BUT_LT, 5), Bounce(BUT_R1, 5), Bounce(BUT_RT, 5), Bounce(BUT_L5, 5), Bounce(BUT_CAM, 5) };
boolean ButtonStates[] = { false, false, false, false, false };
NextionButton nButtons[] = { NextionButton(nex, 1, 9, "bMovementMode", false),NextionButton(nex, 1, 10, "bGait", false),NextionButton(nex, 1, 11, "bAdjustMode", false),NextionButton(nex, 1, 12, "bIsStanding", false) };
NextionVariableNumeric vBattertyLevel(nex, 0, 1, "pGlobals.vBatteryLevel", true);
NextionVariableNumeric vRSSI(nex, 0, 2, "pGlobals.vRSSI", true);
NextionVariableNumeric vMovementMode(nex, 0, 3, "pGlobals.vMovementMode", true);
NextionVariableNumeric vGait(nex, 0, 4, "pGlobals.vGait", true);
NextionVariableNumeric vAdjustMode(nex, 0, 5, "pGlobals.vAdjustMode", true);
NextionVariableNumeric vIsStanding(nex, 0, 6, "pGlobals.vIsStanding", true);
NextionVariableNumeric vIsLightOn(nex, 0, 7, "pGlobals.vIsLightOn", true);
NextionVariableString vOrientationX(nex, 0, 8, "pGlobals.vOrientationX", true);
NextionVariableString vOrientationY(nex, 0, 9, "pGlobals.vOrientationY", true);
NextionVariableString vOrientationZ(nex, 0, 10, "pGlobals.vOrientationZ", true);
NextionPage pShutdown(nex, 7, 0, "pShutdown");


#ifdef DEBUG
boolean g_fDebugOutput;
word g_bButtonsPrev;
byte g_abJoyValsPrev[NUMANALOGS];
#endif

unsigned long ltime;					/* last time we sent data */

int iLoopCnt;

XBee xbee = XBee();
XBeeResponse response = XBeeResponse();
Rx16Response rx16 = Rx16Response();

byte rxData_old[20] = {};
byte rxData[20] = {};

/* The payload is used to transmit the controls to the robot and make it move*/
uint8_t payload[] = { 0,0,0,0,0,0,0,0,0,0,0 };
Tx16Request tx = Tx16Request(XBEE_ADDRESS, payload, sizeof(payload));

/* IMU data vars */
float imuX, imuY, imuZ;
float oldX, oldY, oldZ;

byte floatSource[4];

/* This is used to control XBee IO lines which in turn controls the menu keys for the camera*/
//NextionButton nCamControls[] = { NextionButton(nex, 2, 10, "bDown", false), NextionButton(nex, 2, 12, "bRight", false), NextionButton(nex, 2, 9, "bUp", false), NextionButton(nex, 2, 12, "bLeft", false), NextionButton(nex, 2, 11, "bEnter", false) };
//uint8_t CamCmds[][2] = { {'D', '0'}, {'D', '1'}, {'D', '2'}, {'D','3'}, {'D','4'} };
//uint8_t CamVals[] = { 4, 5 };

//uint8_t uCmd[] = { 'D', '2' }; // XBee IO port 2 pin 18
//uint8_t dCmd[] = { 'D', '0' }; // XBee IO port 0 pin 20
//uint8_t lCmd[] = { 'D', '3' }; // XBee IO port 3 pin 17
//uint8_t rCmd[] = { 'D', '1' }; // XBee IO port 1 pin 19
//uint8_t eCmd[] = { 'D', '4' }; // XBee IO port 4 pin 11
//uint8_t CmdVal[] = { 4, 5 }; // 4=low, 5=high
//RemoteAtCommandRequest racr = RemoteAtCommandRequest(XBEE_ADDRESS, {});

// BatteryMonitor
BatteryMonitor bm(2.63, 3.33, BAT_PROBE, BAT_EN, LOW);
elapsedMillis bmTimer;

uint8_t old_rssi;
elapsedMillis rssiTimer;

elapsedMillis frameTimer;

class Handler : public INextionCallback
{
public:
	Handler()
		: INextionCallback()
	{
	}

	void handleNextionEvent(NextionEventType type, INextionTouchable *widget)
	{
#ifdef DEBUG		
		digitalWrite(USER_LED, !digitalReadFast(USER_LED));
#endif
		DEBUG_PRINT(F("COMPONENT: "));
		DEBUG_PRINTLN(widget->getComponentID());
		switch (nex.getCurrentPage())
		{
		case 1:
			for (int i = 0; i < 4; i++) {
				if (nButtons[i].getName() == widget->getName()) {
					ButtonStates[i] = true;
					break;
				}
			}
			break;
		case 2:
			//for (int i = 0; i < 5; i++) {
			//	if (nCamControls[i].getName() == widget->getName()) {
			//		toggleXBeeIO(i);
			//		break;
			//	}
			//}
			break;
		}
	}
};

Handler handle;

void setup()
{
#ifdef DEBUG || DEBUG_BTN || DEBUG_BAT
	SER_DBG.begin(115200);
#endif
	pinMode(USER_LED, OUTPUT);

	SER_XB.begin(38400);
	xbee.setSerial(SER_XB);

	NEXTION_PORT.begin(38400);
	nex.init();

	for (int i = 0; i < 4; i++) {
		nButtons[i].attachCallback(&handle);
	}
	//for (int i = 0; i < 5; i++) {
	//	nCamControls[i].attachCallback(&handle);
	//}

	bm.begin(3.33, 33, 12);

	checkBattery(true);

	// init physical buttons and joysticks
	initButtons();
	initJoysticks();

	frameTimer = 0;
}

void loop()
{
	nex.poll();

	if (frameTimer > FRAME_LEN)
	{
		byte i;
		int bChksum;

		readJoysticks();

		readButtons();

		if (iLoopCnt > 10) {
			digitalWrite(USER_LED, HIGH - digitalRead(USER_LED));
			iLoopCnt = 0;
		}
		iLoopCnt++;

		// Compute our checksum...
		bChksum = (int)g_bButtons;

		for (i = 0; i < NUMANALOGS; i++)
			bChksum += (int)g_abJoyVals[i];

		bChksum = (byte)(255 - (byte)(bChksum % 256));

#ifdef DEBUG_BTN
		if ((g_bButtons != g_bButtonsPrev) || memcmp(&g_abJoyVals, &g_abJoyValsPrev, sizeof(g_abJoyVals)))
		{
			g_bButtonsPrev = g_bButtons;
			memcpy(&g_abJoyValsPrev, &g_abJoyVals, sizeof(g_abJoyVals));
			SER_DBG.print(g_bButtons, HEX);
			SER_DBG.print(F(" : "));
			for (i = 0; i < NUMANALOGS; i++)
			{
				SER_DBG.print((int)g_abJoyVals[i] - 128, DEC);
				SER_DBG.print(F(" "));
			}
			SER_DBG.println(bChksum, DEC);
		}
#endif

		byte pi = 0;
		payload[0] = (byte)0xff;
		for (i = 0; i < NUMANALOGS; i++)
		{
			pi = i;
			pi++;
			payload[pi] = (byte)g_abJoyVals[i];
		}
		payload[7] = (byte)highByte(g_bButtons);
		payload[8] = (byte)lowByte(g_bButtons);
		payload[9] = (byte)0;
		payload[10] = (byte)(byte)bChksum;

		xbee.send(tx);
	
		frameTimer = 0;
	}
	readBouncedButtons();

	checkBattery(false);

	readPackets();

	//delay(FRAME_LEN);
}

void readJoysticks()
{
	byte i;

	// Calculate which index of our raw array we should now reuse.
	g_iRaw = (g_iRaw + 1) & 0x7;

	// Now lets read in the next analog reading and smooth the values
	// Note: The original commander code did not allow a full range, so we will not as well
	// See if I get away with 1-254
	for (i = 0; i<NUMANALOGS; i++)
	{
		g_awRawSums[i] -= g_awRawAnalog[g_iRaw][i];        // remove the value we overwrite from sum
		g_awRawAnalog[g_iRaw][i] = analogRead(i);
		g_awRawSums[i] += g_awRawAnalog[g_iRaw][i];        // Add the new value in

														   // Lets calculate our calibrated values from 0-255 whith 128 as center point
		if (g_awRawSums[i] <= g_awAnalogMins[i])
			g_abJoyVals[i] = 1;                // Take care of out of range low
		else if (g_awRawSums[i] >= g_awAnalogMaxs[i])
			g_abJoyVals[i] = 254;              // out of range high
		else if (g_awRawSums[i] <= g_awAnalogMids[i])
			g_abJoyVals[i] = map(g_awRawSums[i], g_awAnalogMins[i], g_awAnalogMids[i], 1, 128);
		else
			g_abJoyVals[i] = map(g_awRawSums[i], g_awAnalogMids[i], g_awAnalogMaxs[i], 128, 254);

		if (g_afAnalogInvert[i])
			g_abJoyVals[i] = 255 - g_abJoyVals[i];
	}
}

/* Read buttons */
void readButtons()
{
	g_bButtons = 0;

	//if (digitalRead(BUT_R1) == HIGH || ButtonStates[1] == true) // BUT_R1
	if (ButtonStates[1] == true) // BUT_R1
	{
		g_bButtons += 1;
		ButtonStates[1] = false;
	}
	if (digitalRead(BUT_R2) == HIGH) g_bButtons += 2;
	if (digitalRead(BUT_R3) == HIGH) g_bButtons += 4;
	if (digitalRead(BUT_L6) == HIGH) g_bButtons += 8;
//	if (digitalRead(BUT_L5) == HIGH || ButtonStates[3] == true) // BUT_L5
	if (ButtonStates[3] == true) // BUT_L5
	{
		g_bButtons += 16;
		ButtonStates[3] = false;
	}
	if (digitalRead(BUT_L4) == HIGH) g_bButtons += 32;
//	if (digitalRead(BUT_RT) == LOW || ButtonStates[2] == true) // BUT_RT
	if (ButtonStates[2] == true) // BUT_RT
	{
		g_bButtons += 64;
		ButtonStates[2] = false;
	}
//	if (digitalRead(BUT_LT) == LOW || ButtonStates[0] == true)  // BUT_LT
	if (ButtonStates[0] == true)  // BUT_LT
	{
		g_bButtons += 128;
		ButtonStates[0] = false;
	}
	//if (digitalRead(BUT_CAM) == LOW) g_bButtons += 256;
	if (ButtonStates[4] == true)
	{
		g_bButtons += 256;
		ButtonStates[4] = false;
	}
}

void readBouncedButtons()
{
	for (int i = 0; i < 5; i++) {
		bButtons[i].update();
		if ( (i % 2 == 0 && bButtons[i].fell()) || (i % 2 == 1 && bButtons[i].rose()) ) {
			ButtonStates[i] = true;
			
			//if (i < 4)
			//{
			//	size_t commandLen = 9 + strlen(nButtons[i].getName());
			//	char commandBuffer[commandLen];
			//	snprintf(commandBuffer, commandLen, "click %s,0", nButtons[i].getName());
			//	nex.sendCommand(commandBuffer);
			//}
		}
	}
}
//==================================
// Battery functions
//==================================

void checkBattery(bool skipTimer)
{
	if (bmTimer > 5000 || skipTimer)
	{
		bm.measure();

#ifdef DEBUG_BAT
		bm.printData(SER_DBG);
#endif
		vBattertyLevel.setValue(bm.level());

#ifndef DEBUG
		if (bm.isLow())
		{
#if defined(DEBUG) || defined(DEBUG_BAT)
			SER_DBG.println("!!! BATTERY LOW, SHUTDOWN!!!");
#endif
			if ( !pShutdown.show() ) nex.sendCommand(shutdownPage);

			while (true)
			{
				if (iLoopCnt > 10) {
					digitalWrite(USER_LED, HIGH - digitalRead(USER_LED));
					iLoopCnt = 0;
				}
				iLoopCnt++;

				delay(250);
			}

		}
#endif
		bmTimer = 0;
	}
}

/*
Packet received should be 20 bytes with the following format
Position	Represents
--------	----------
0			Standing/Sitting
1			Control mode
2			Speed/height mode
3			Current gait
4			Lights on/off
5			Axis marker X
6,7,8,9		X float value
10			Axis marker Y
11,12,13,14	Y float value
15			Axis marker Z
16,17,18,19	Z float value
*/

void readPackets()
{
	xbee.readPacket();

	if (xbee.getResponse().isAvailable()) {
		if (xbee.getResponse().getApiId() == RX_16_RESPONSE) {
			xbee.getResponse().getRx16Response(rx16);

			for (uint8_t i = 0; i < rx16.getDataLength(); i++) {
				rxData[i] = rx16.getData(i);
			}

			if (rxData[0] != rxData_old[0])
			{
				vIsStanding.setValue(rxData[0]);
				rxData_old[0] = rxData[0];
			}
			if (rxData[1] != rxData_old[1])
			{
				vMovementMode.setValue(rxData[1]);
				rxData_old[1] = rxData[1];
			}
			if (rxData[2] != rxData_old[2])
			{
				vAdjustMode.setValue(rxData[2]);
				rxData_old[2] = rxData[2];
			}
			if (rxData[3] != rxData_old[3])
			{
				vGait.setValue(rxData[3]);
				rxData_old[3] = rxData[3];
			}
			if (rxData[4] != rxData_old[4])
			{
				vIsLightOn.setValue(rxData[4]);
				rxData_old[4] = rxData[4];
			}
			
			imuX = float(long(rxData[9] + (rxData[8] << 8) + (rxData[7] << 16) + (rxData[6] << 24)) / 1000.0);
			imuY = float(long(rxData[14] + (rxData[13] << 8) + (rxData[12] << 16) + (rxData[11] << 24)) / 1000.0);
			imuZ = float(long(rxData[19] + (rxData[18] << 8) + (rxData[17] << 16) + (rxData[16] << 24)) / 1000.0);

			if (imuX != oldX) {
				String xs = String(imuX, 4);
				char xb[10] = {};
				xs.toCharArray(xb, xs.length());
				vOrientationX.setText(xb);
				oldX = imuX;
			}

			if (imuY != oldY) {
				String ys = String(imuY, 4);
				char yb[10] = {};
				ys.toCharArray(yb, ys.length());
				vOrientationY.setText(yb);
				oldY = imuY;
			}

			if (imuZ != oldZ) {
				String zs = String(imuX, 4);
				char zb[10] = {};
				zs.toCharArray(zb, zs.length());
				vOrientationZ.setText(zb);
				oldZ = imuZ;
			}

			//// imuX parsing
			//for (uint8_t cnt = 0; cnt < 4; cnt++) {
			//	floatSource[cnt] = rxData[6 + cnt];
			//}
			//memcpy( &imuX, &floatSource, 4);
			//DEBUG_PRINTLN(imuX);
			//String xs = String(imuX, 4);
			//char xb[xs.length()];
			//xs.toCharArray(xb, xs.length());
			//vOrientationX.setText(xb);

			//// imuY parsing
			//for (uint8_t cnt = 0; cnt < 4; cnt++) {
			//	floatSource[cnt] = rxData[11 + cnt];
			//}
			//memcpy(&imuY, &floatSource, 4);
			//DEBUG_PRINTLN(imuY);
			//String ys = String(imuY, 4);
			//char yb[ys.length()];
			//ys.toCharArray(yb, ys.length());
			//vOrientationY.setText(yb);

			//// imuZ parsing
			//for (uint8_t cnt = 0; cnt < 4; cnt++) {
			//	floatSource[cnt] = rxData[16 + cnt];
			//}
			//memcpy(&imuZ, &floatSource, 4);
			//DEBUG_PRINTLN(imuZ);
			//String zs = String(imuZ, 4);
			//char zb[zs.length()];
			//zs.toCharArray(zb, zs.length());
			//vOrientationZ.setText(zb);


			uint8_t currentRssi = rx16.getRssi();

			if (currentRssi != old_rssi && rssiTimer > 1000)
			{
				DEBUG_PRINTLN(currentRssi);
				vRSSI.setValue(100 - currentRssi);
				old_rssi = currentRssi;
				rssiTimer = 0;
			}
		}
	}
	else if (xbee.getResponse().isError()) {
		DEBUG_PRINTLN(F("ERROR RESPONSE"));
	}
}

void initJoysticks()
{
	byte i;
	//word w;

	// We need to prefill our array of raw items and sums.  The first read for each analog
	// may be trash, but that will get removed on our first real read.
	for (i = 0; i<NUMANALOGS; i++) {
		g_awRawSums[i] = 0;        // initialize to zero
								   //w = analogRead(i);    // first read is not valid
		analogRead(i);
		for (g_iRaw = 0; g_iRaw < 8; g_iRaw++) {
			g_awRawAnalog[g_iRaw][i] = analogRead(i);
			g_awRawSums[i] += g_awRawAnalog[g_iRaw][i];
			delay(1);
		}
		// Save away these sums as our mid points...
		g_awAnalogMids[i] = g_awRawSums[i];
	}
}

void initButtons()
{
	pinMode(BUT_L6, INPUT);
	pinMode(BUT_L5, INPUT);
	pinMode(BUT_L4, INPUT);

	pinMode(BUT_R1, INPUT);
	pinMode(BUT_R2, INPUT);
	pinMode(BUT_R3, INPUT);

	pinMode(BUT_LT, INPUT_PULLUP);
	pinMode(BUT_RT, INPUT_PULLUP);
	pinMode(BUT_CAM, INPUT_PULLUP);

	digitalWrite(BUT_L6, LOW);
	digitalWrite(BUT_L5, LOW);
	digitalWrite(BUT_L4, LOW);

	digitalWrite(BUT_R1, LOW);
	digitalWrite(BUT_R2, LOW);
	digitalWrite(BUT_R3, LOW);

	digitalWrite(BUT_LT, HIGH);
	digitalWrite(BUT_RT, HIGH);
	digitalWrite(BUT_CAM, HIGH);
}

void toggleXBeeIO(uint8_t idx)
{
	//racr.setApplyChanges(true);
	//racr.setCommand(CamCmds[idx]);
	//racr.setCommandValue(&CamVals[0]);
	//racr.setCommandValueLength(1);
	//xbee.send(racr);
	//racr.clearCommandValue();

	//delay(100);

	//racr.setCommandValue(&CamVals[1]);
	//racr.setCommandValueLength(1);
	//xbee.send(racr);
}