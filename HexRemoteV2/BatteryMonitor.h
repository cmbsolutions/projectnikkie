// BatteryMonitor.h

#ifndef _BATTERYMONITOR_
#define _BATTERYMONITOR_

#include "arduino.h"

class BatteryMonitor {
public:
	BatteryMonitor(float minVoltage, float maxVoltage, uint8_t measurePin, uint8_t enablePin, bool enableState = HIGH);

	void begin(float refVoltage = 3.33, uint8_t r1 = 33, uint8_t r2 = 12);

	void measure();

	int level();

	float voltage();

	bool isLow();

	void printData(Stream &serial);

private:
	float _refVoltage;
	float _minVoltage;
	float _maxVoltage;
	float _r1, _r2, _out;
	float _ratio, _adcRef, _refRatio, _pinVoltage, _batteryVoltage;
	uint8_t _measurePin;
	uint8_t _enablePin;
	uint8_t _sampleCount = 5;
	uint16_t _sampleData;
	bool _enableState;
	bool _useEnable = false;
};

#endif
