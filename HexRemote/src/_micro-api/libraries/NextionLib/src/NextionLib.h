// NextionLib.h

#ifndef _NEXTIONLIB_
#define _NEXTIONLIB_

#include "arduino.h"

class NextionLib {
private:
	void flushSerial();
	HardwareSerial *nextion;

public:
	NextionLib() {};//Empty constructor
	NextionLib(HardwareSerial &next);

	//Constructor
	void begin(uint32_t baud);

	void buttonToggle(boolean &buttonState, String objName, uint8_t picDefualtId, uint8_t picPressedId);

	uint8_t buttonOnOff(String find_component, String unknown_component, uint8_t pin, int btn_prev_state);

	boolean setComponentValue(String component, int value);

	//boolean ack(void);//Deprecated

	boolean ack(void);

	unsigned int getComponentValue(String component);

	boolean setComponentText(String component, String txt);

	boolean updateProgressBar(int x, int y, int maxWidth, int maxHeight, int value, int emptyPictureID, int fullPictureID, int orientation = 0);

	String getComponentText(String component, uint32_t timeout = 100);

	String listen(unsigned long timeout = 100);
	//  String listenNextionGeneric(unsigned long timeout=100);

	void sendCommand(const char* cmd);

	uint8_t pageId(void);

	boolean init(const char* pageId = "0");

};

#endif

