// 
// 
// 

#include "BatteryMonitor.h"

BatteryMonitor::BatteryMonitor(float minVoltage, float maxVoltage, uint8_t measurePin, uint8_t enablePin, bool enableState)
{
	this->_minVoltage = minVoltage;
	this->_maxVoltage = maxVoltage;
	this->_measurePin = measurePin;
	this->_enablePin = enablePin;
	this->_enableState = enableState;
	this->_useEnable = true;
}

void BatteryMonitor::begin(float refVoltage, uint8_t r1, uint8_t r2)
{
	this->_refVoltage = refVoltage;
	this->_r1 = r1;
	this->_r2 = r2;

	this->_out = (this->_r2 / (this->_r1 + this->_r2)) * this->_maxVoltage;
	this->_ratio = this->_maxVoltage / this->_out;
	this->_adcRef = this->_refVoltage / 1024;
	this->_refRatio = this->_out / this->_refVoltage;

	if (this->_useEnable)
	{
		pinMode(this->_enablePin, OUTPUT);
		digitalWrite(this->_enablePin, !this->_enableState);
		analogReadResolution(10);
		analogRead(this->_measurePin);
	}
}

int BatteryMonitor::level()
{
	return (int)map((this->_pinVoltage * 1000), (this->_minVoltage * 1000), (this->_maxVoltage * 1000), 0, 100);
}

float BatteryMonitor::voltage()
{
	return this->_batteryVoltage;
}

bool BatteryMonitor::isLow()
{
	if (this->_minVoltage >= this->_pinVoltage)
		return true;

	return false;
}

void BatteryMonitor::measure()
{
	this->_sampleData = 0;

	digitalWrite(this->_enablePin, this->_enableState);
	delay(5);
	
	for (uint8_t i = 0; i < this->_sampleCount; i++)
	{
		this->_sampleData += analogRead(this->_measurePin);
		delay(10);
	}
	digitalWrite(this->_enablePin, !this->_enableState);

	this->_pinVoltage = (this->_sampleData / this->_sampleCount) * this->_adcRef;
	this->_batteryVoltage = this->_pinVoltage * this->_ratio;
}
