//==================================
// Includes
//==================================

#include "config.h"

#include <XBee.h>
#include <Nextion.h>

#include "BatteryMonitor.h"
#include <elapsedMillis.h>
#include <Bounce2.h>

//==================================
// Consants
//==================================

static const boolean g_afAnalogInvert[] = { true, true, true, true, true, true };
static const word g_awAnalogMins[] = { 0 * 8, 0 * 8, 0 * 8, 0 * 8, 0 * 8, 0 * 8 };
static const word g_awAnalogMaxs[] = { 1023 * 8, 1023 * 8, 1023 * 8, 1023 * 8, 1023 * 8, 1023 * 8 };


//==================================
// Globals
//==================================

uint8_t g_fLimitRange = false;
word g_awRawAnalog[8][NUMANALOGS];		/* Keep 8 raw values for each analog input */
word g_awRawBatAnalog[8];
word g_awRawSums[NUMANALOGS];			/* sum of the raw values */
word g_awAnalogMids[NUMANALOGS];		/* This is our observed center points at init time */ 
byte g_iRaw;							/* which raw value are we going to read into */

word g_bButtons;
word g_abJoyVals[NUMANALOGS];			/* Joystick values */

#ifdef DEBUG
boolean g_fDebugOutput;
byte g_bButtonsPrev;
byte g_abJoyValsPrev[NUMANALOGS];
#endif

unsigned long ltime;					/* last time we sent data */

int iLoopCnt;

// XBee API
XBee xbee = XBee();
XBeeResponse response = XBeeResponse();
Rx16Response rx16 = Rx16Response();
Rx64Response rx64 = Rx64Response();

uint8_t option = 0;
uint8_t data = 0;
uint8_t rssi = 0;

/* The payload is used to transmit the controls to the robot and make it move*/
uint8_t payload[] = { 0,0,0,0,0,0,0,0,0,0,0 };
Tx16Request tx = Tx16Request(XBEE_ADDRESS, payload, sizeof(payload));

/* This is used to control XBee IO lines which in turn controls the menu keys for the camera*/
uint8_t uCmd[] = { 'D', '2' }; // XBee IO port 2 pin 18
uint8_t dCmd[] = { 'D', '0' }; // XBee IO port 0 pin 20
uint8_t lCmd[] = { 'D', '3' }; // XBee IO port 3 pin 17
uint8_t rCmd[] = { 'D', '1' }; // XBee IO port 1 pin 19
uint8_t eCmd[] = { 'D', '4' }; // XBee IO port 4 pin 11

uint8_t lowVal[] = { 4 };
uint8_t highVal[] = { 5 };
	
RemoteAtCommandRequest racr = RemoteAtCommandRequest(XBEE_ADDRESS, {});


// BatteryMonitor
BatteryMonitor bm(2.63, 3.33, BAT_PROBE, BAT_EN, LOW);
String bmScreenCommand = "pShutdown";

// Nextion touchscreen handlers etc
NexButton bU = NexButton(2, 9, "bUp");
NexButton bD = NexButton(2, 10, "bDown");
NexButton bL = NexButton(2, 12, "bLeft");
NexButton bR = NexButton(2, 13, "bRight");
NexButton bE = NexButton(2, 11, "bEnter");

NexButton bvL5 = NexButton(1, 12, "L5");
NexButton bvLT = NexButton(1, 9, "LT");
NexButton bvR1 = NexButton(1, 10, "R1");
NexButton bvRT = NexButton(1, 11, "RT");
/* These are used to register what virtual button on nextion is pushed*/
bool bvL5Pushed = false;
bool bvLTPushed = false;
bool bvR1Pushed = false;
bool bvRTPushed = false;
/* These will be used to tie the physical buttons to the virtual ones */
Bounce bpL5 = Bounce(BUT_L5, 25);
Bounce bpLT = Bounce(BUT_LT, 25);
Bounce bpR1 = Bounce(BUT_R1, 25);
Bounce bpRT = Bounce(BUT_RT, 25);


NexTouch *nex_listen_list[] =
{
	&bU,
	&bD,
	&bL,
	&bR,
	&bE,
	&bvL5,
	&bvLT,
	&bvR1,
	&bvRT,
	NULL
};


// Timers
elapsedMillis tmr;

void setup()
{

#if defined(DEBUG) || defined(DEBUG_BAT) || defined(DEBUG_BTN)
	SER_DBG.begin(38400);
	SER_DBG.println("HexRemote debug mode ready...");
#endif

	//nexInit();
	nexSerial.begin(9600);
	delay(250);
	sendCommand("");
	sendCommand("bkcmd=3");
	bool ret1 = recvRetCommandFinished();

	// Start with battery monitoring... If low power we wont continue!
	bm.begin(3.33, 33, 12);

	checkBattery(true);

	// init physical buttons and joysticks
	initButtons();
	initJoysticks();

	// Attach virtual buttons
	bU.attachPop(bUPopCallback, &bU);
	bD.attachPop(bDPopCallback, &bD);
	bL.attachPop(bLPopCallback, &bL);
	bR.attachPop(bRPopCallback, &bR);
	bE.attachPop(bEPopCallback, &bE);
	bvL5.attachPop(bL5PopCallback, &bvL5);
	bvLT.attachPop(bLTPopCallback, &bvLT);
	bvR1.attachPop(bR1PopCallback, &bvR1);
	bvRT.attachPop(bRTPopCallback, &bvRT);

#ifdef DEBUG
	SER_DBG.println("Nextion buttons attached");
#endif

	SER_XB.begin(38400);
	xbee.setSerial(SER_XB);
	
	// This is the signal strength on the nextion
	// TODO: Change to actual XBee RSSI value
	setNexVarValue("va1", 100);

	ltime = millis();
	pinMode(USER_LED, OUTPUT);

	iLoopCnt = 0;
}

void loop()
{
	byte i;
	int bChksum;

	nexLoop(nex_listen_list);

	readJoysticks();

	readButtons();

	if (iLoopCnt > 10) {
		digitalWrite(USER_LED, HIGH - digitalRead(USER_LED));
		iLoopCnt = 0;
	}
	iLoopCnt++;

	// Compute our checksum...
	bChksum = (int)g_bButtons;

	for (i = 0; i < NUMANALOGS; i++)
		bChksum += (int)g_abJoyVals[i];
	
	bChksum = (byte)(255 - (byte)(bChksum % 256));

#ifdef DEBUG_BTN
	if ((g_bButtons != g_bButtonsPrev) || memcmp(&g_abJoyVals, &g_abJoyValsPrev, sizeof(g_abJoyVals)))
	{
		g_bButtonsPrev = g_bButtons;
		memcpy(&g_abJoyValsPrev, &g_abJoyVals, sizeof(g_abJoyVals));
		SER_DBG.print(g_bButtons, HEX);
		SER_DBG.print(F(" : "));
		for (i = 0; i < NUMANALOGS; i++)
		{
			SER_DBG.print((int)g_abJoyVals[i] - 128, DEC);
			SER_DBG.print(F(" "));
		}
		SER_DBG.println(bChksum, DEC);
	}
#endif

	byte pi = 0;
	payload[0] = (byte)0xff;
	for (i = 0; i < NUMANALOGS; i++)
	{
		pi = i;
		pi++;
		payload[pi] = (byte)g_abJoyVals[i];
	}
	payload[7] = (byte)highByte(g_bButtons);
	payload[8] = (byte)lowByte(g_bButtons);
	payload[9] = (byte)0;
	payload[10] = (byte)(byte)bChksum;

	xbee.send(tx);

	readBouncedButtons();

	checkBattery(false);

	delay(FRAME_LEN);
}

//==================================
// Nextion button callbacks
//==================================
void bUPopCallback(void *ptr)
{
#ifdef DEBUG
	SER_DBG.print("bUp:");
	SER_DBG.println((uint32_t)ptr);
#endif

	toggleRemoteIO(uCmd);
}

void bDPopCallback(void *ptr)
{
#ifdef DEBUG
	SER_DBG.print("bDown:");
	SER_DBG.println((uint32_t)ptr);
#endif

	toggleRemoteIO(dCmd);
}

void bLPopCallback(void *ptr)
{
#ifdef DEBUG
	SER_DBG.print("bLeft:");
	SER_DBG.println((uint32_t)ptr);
#endif

	toggleRemoteIO(lCmd);
}

void bRPopCallback(void *ptr)
{
#ifdef DEBUG
	SER_DBG.print("bRight:");
	SER_DBG.println((uint32_t)ptr);
#endif

	toggleRemoteIO(rCmd);
}

void bEPopCallback(void *ptr)
{
#ifdef DEBUG
	SER_DBG.print("bEnter:");
	SER_DBG.println((uint32_t)ptr);
#endif

	toggleRemoteIO(eCmd);
}

void bL5PopCallback(void *ptr)
{
#ifdef DEBUG
	SER_DBG.print("bL5:");
	SER_DBG.println((uint32_t)ptr);
#endif

	bvL5Pushed = true;
}

void bLTPopCallback(void *ptr)
{
#ifdef DEBUG
	SER_DBG.print("bLT:");
	SER_DBG.println((uint32_t)ptr);
#endif

	bvLTPushed = true;
}

void bR1PopCallback(void *ptr)
{
#ifdef DEBUG
	SER_DBG.print("bR1:");
	SER_DBG.println((uint32_t)ptr);
#endif

	bvR1Pushed = true;
}

void bRTPopCallback(void *ptr)
{
#ifdef DEBUG
	SER_DBG.print("bRT:");
	SER_DBG.println((uint32_t)ptr);
#endif

	bvRTPushed = true;
}


//==================================
// Battery functions
//==================================

void checkBattery(bool skipTimer)
{
	if (tmr > 5000 || skipTimer)
	{
		bm.measure();

#ifdef DEBUG_BAT
		SER_DBG.print("Voltage: ");
		SER_DBG.print(bm.voltage());
		SER_DBG.print(" (");
		SER_DBG.print(bm.level());
		SER_DBG.println("%)");
#endif

		setNexVarValue("va0", bm.level());

#ifndef DEBUG
		if (bm.isLow())
		{
#if defined(DEBUG) || defined(DEBUG_BAT)
			SER_DBG.println("!!! BATTERY LOW, SHUTDOWN!!!");
#endif
			sendCommand(bmScreenCommand.c_str());

			
			//SER_XB.end();
			//nexSerial.end();
			
			while (true)
			{
				if (iLoopCnt > 10) {
					digitalWrite(USER_LED, HIGH - digitalRead(USER_LED));
					iLoopCnt = 0;
				}
				iLoopCnt++;

				delay(250);
			}
		
		}
#endif
		tmr = 0;
	}
}


void setNexVarValue(String component, int value) {
	String compValue = component + ".val=" + value;//Set component value
	sendCommand(compValue.c_str());
}

void initJoysticks()
{
	byte i;
	//word w;

	// We need to prefill our array of raw items and sums.  The first read for each analog
	// may be trash, but that will get removed on our first real read.
	for (i = 0; i<NUMANALOGS; i++) {
		g_awRawSums[i] = 0;        // initialize to zero
		//w = analogRead(i);    // first read is not valid
		analogRead(i);
		for (g_iRaw = 0; g_iRaw < 8; g_iRaw++) {
			g_awRawAnalog[g_iRaw][i] = analogRead(i);
			g_awRawSums[i] += g_awRawAnalog[g_iRaw][i];
			delay(1);
		}
		// Save away these sums as our mid points...
		g_awAnalogMids[i] = g_awRawSums[i];
	}
}

/* Initialize buttons and set the default state */
void initButtons()
{
	pinMode(BUT_L6, INPUT);
	pinMode(BUT_L5, INPUT);
	pinMode(BUT_L4, INPUT);

	pinMode(BUT_R1, INPUT);
	pinMode(BUT_R2, INPUT);
	pinMode(BUT_R3, INPUT);

	pinMode(BUT_LT, INPUT_PULLUP);
	pinMode(BUT_RT, INPUT_PULLUP);
	pinMode(BUT_CAM, INPUT_PULLUP);

	digitalWrite(BUT_L6, LOW);
	digitalWrite(BUT_L5, LOW);
	digitalWrite(BUT_L4, LOW);

	digitalWrite(BUT_R1, LOW);
	digitalWrite(BUT_R2, LOW);
	digitalWrite(BUT_R3, LOW);

	digitalWrite(BUT_LT, HIGH);
	digitalWrite(BUT_RT, HIGH);
	digitalWrite(BUT_CAM, HIGH);
}


void readJoysticks()
{
	byte i;

	// Calculate which index of our raw array we should now reuse.
	g_iRaw = (g_iRaw + 1) & 0x7;

	// Now lets read in the next analog reading and smooth the values
	// Note: The original commander code did not allow a full range, so we will not as well
	// See if I get away with 1-254
	for (i = 0; i<NUMANALOGS; i++)
	{
		g_awRawSums[i] -= g_awRawAnalog[g_iRaw][i];        // remove the value we overwrite from sum
		g_awRawAnalog[g_iRaw][i] = analogRead(i);
		g_awRawSums[i] += g_awRawAnalog[g_iRaw][i];        // Add the new value in

															// Lets calculate our calibrated values from 0-255 whith 128 as center point
		if (g_awRawSums[i] <= g_awAnalogMins[i])
			g_abJoyVals[i] = 1;                // Take care of out of range low
		else if (g_awRawSums[i] >= g_awAnalogMaxs[i])
			g_abJoyVals[i] = 254;              // out of range high
		else if (g_awRawSums[i] <= g_awAnalogMids[i])
			g_abJoyVals[i] = map(g_awRawSums[i], g_awAnalogMins[i], g_awAnalogMids[i], 1, 128);
		else
			g_abJoyVals[i] = map(g_awRawSums[i], g_awAnalogMids[i], g_awAnalogMaxs[i], 128, 254);

		if (g_afAnalogInvert[i])
			g_abJoyVals[i] = 255 - g_abJoyVals[i];
	}
}

/* Read buttons */
void readButtons()
{
	g_bButtons = 0;

	if (digitalRead(BUT_R1) == HIGH || bvR1Pushed)
	{
		g_bButtons += 1;
		bvR1Pushed = false;
	}
	if (digitalRead(BUT_R2) == HIGH) g_bButtons += 2;
	if (digitalRead(BUT_R3) == HIGH) g_bButtons += 4;
	if (digitalRead(BUT_L6) == HIGH) g_bButtons += 8;
	if (digitalRead(BUT_L5) == HIGH || bvL5Pushed)
	{
		g_bButtons += 16;
		bvL5Pushed = false;
	}
	if (digitalRead(BUT_L4) == HIGH) g_bButtons += 32;
	if (digitalRead(BUT_RT) == LOW || bvRTPushed)
	{
		g_bButtons += 64;
		bvRTPushed = false;
	}
	if (digitalRead(BUT_LT) == LOW || bvLTPushed)
	{
		g_bButtons += 128;
		bvLTPushed = false;
	}
	if (digitalRead(BUT_CAM) == LOW) g_bButtons += 256;
}

void clickNexButton(String component, int value) {
	String compValue = "click " + component + "," + value;//Set component value
	sendCommand(compValue.c_str());
}

void readBouncedButtons()
{
	bpL5.update();
	if (bpL5.rose()) clickNexButton("L5", 1);
	bpLT.update();
	if (bpLT.fell()) clickNexButton("LT", 1);
	bpR1.update();
	if (bpR1.rose()) clickNexButton("R1", 1);
	bpRT.update();
	if (bpRT.fell()) clickNexButton("RT", 1);
}

void toggleRemoteIO(uint8_t ioPort[])
{
	racr.setApplyChanges(true);
	racr.setCommand(ioPort);
	racr.setCommandValue(highVal);
	racr.setCommandValueLength(sizeof(highVal));
	xbee.send(racr);
	racr.clearCommandValue();

	delay(100);

	racr.setApplyChanges(true);
	racr.setCommand(ioPort);
	racr.setCommandValue(lowVal);
	racr.setCommandValueLength(sizeof(lowVal));
	xbee.send(racr);
	racr.clearCommandValue();
}
