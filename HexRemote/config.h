#pragma once

//==================================
// Defines
//==================================

// Options

#define DEBUG
#define DEBUG_BTN
#define DEBUG_BAT

// Joysticks
#define RIGHT_V A3			/* pin 17 */
#define RIGHT_H A2			/* pin 16 */
#define LEFT_V A1			/* pin 15 */
#define LEFT_H A0			/* pin 14 */

#define CAM_V A5			/* pin 19 */
#define CAM_H A4			/* pin 18 */

// Buttons
#define BUT_L6 4			/* pin 3, left red */
#define BUT_L5 2			/* pin 4, left green */
#define BUT_L4 3			/* pin 2, left blue */
#define BUT_LT 12			/* pin 12, left joystick button */

#define BUT_R1 5			/* pin 5, right red */
#define BUT_R2 6			/* pin 6, right green */
#define BUT_R3 9			/* pin 9, right blue */
#define BUT_RT 10			/* pin 10, right bottom joystick button */

#define BUT_CAM 11			/* pin 11, right top joystick button */

// Communications
#define SER_DBG Serial		/* usb */
#define SER_XB Serial1		/* pin 0, 1 */
//#ifndef nexSerial
#define nexSerial Serial3		/* pin 7, 8 */
//#endif

// Etc
#define USER_LED 13			/* pin 13 */
#define FRAME_LEN 33		/* 30Hz */
#define NUMANALOGS 6		/* Number of analog axis*/

#define BAT_EN 20			/* pin 23, battery probe enable */
#define BAT_PROBE A7		/* pin 22, battery probe */

#define XBEE_ADDRESS 0x0002