/* =============================================================================
/ Project N.I.K.K.I.E.
/ Description: Phoenix HexaPod code, converted to PhantomX by
/              Xan, KurtE and Zenta and rebuild for Teensy 3.6 by CMBSolutions
/ Software version: 1.0
/ Date: 13-08-2018
/ Programmer: CMBSolutions
/
/=============================================================================
/ Header Files
/=============================================================================
*/

#define HEXMODE   // default to hex mode
#define DISPLAY_GAIT_NAMES
#define DEFINE_HEX_GLOBALS

#include <Arduino.h>
#include <EEPROM.h>
#include <avr/pgmspace.h>

#include <Wire.h>
#include <SPI.h>

//#include <Pixy.h>

#include <XBee.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <elapsedMillis.h>

#include <Audio.h>
#include <SD.h>
#include <Utility\Sd2Card.h>
#include <SerialFlash.h>

#include "Hex_Cfg.h"

#include "ax12serial.h"
#include "_Phoenix.h"

#include "_Phoenix_Input_Commander.h"
#include "_Phoenix_Driver_AX12.h"
#include "_Phoenix_Code.h"

