/*
	Name:       AudioTest2.ino
	Created:	30-10-2018 21:21:51
	Author:     GOLLEM\Maurice
*/
//#define USE_TEENSY3_OPTIMIZED_CODE


#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <Utility\Sd2Card.h>
#include <SerialFlash.h>

#define SDCARD_CS_PIN    BUILTIN_SDCARD
#define SDCARD_MOSI_PIN  11  // not actually used
#define SDCARD_SCK_PIN   13  // not actually used


// GUItool: begin automatically generated code
AudioPlaySdWav           playSdWav1;     //xy=393,290
AudioMixer4              mixer1;         //xy=645,350
AudioOutputAnalog        dac1;           //xy=895,346
AudioConnection          patchCord1(playSdWav1, 0, mixer1, 0);
AudioConnection          patchCord2(playSdWav1, 1, mixer1, 1);
AudioConnection          patchCord5(mixer1, dac1);
// GUItool: end automatically generated code

char buffer[3];

void setup()
{
	Serial.begin(115200);

	if (!(SD.begin(SDCARD_CS_PIN))) {
		// stop here, but print a message repetitively
		while (1) {
			Serial.println("Unable to access the SD card");
			delay(500);
		}
	}

	Serial.println("Ready to play, send 001 to 018");
	AudioMemory(15);
	
	mixer1.gain(0, 1.0);
	mixer1.gain(1, 1.0);
}

void loop()
{
	if (Serial.available() > 0)
	{
		String audioFile = Serial.readString(3);
		audioFile.concat(".wav");

		Serial.println(audioFile);
		
		playSdWav1.play(audioFile.c_str());

	}
}
