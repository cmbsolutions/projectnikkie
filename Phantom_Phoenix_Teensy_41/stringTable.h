#pragma once


const char string_0[] PROGMEM = "Hi, i am project __nikkie.";
const char string_1[] PROGMEM = "All systems are working normaly.";
const char string_2[] PROGMEM = "Double height is now active.";
const char string_3[] PROGMEM = "Double travel is now active.";
const char string_4[] PROGMEM = "GP Player mode selected.";
const char string_5[] PROGMEM = "Hi, i am project __nikkie.";
const char string_6[] PROGMEM = "Normal walk mode selected.";
const char string_7[] PROGMEM = "Switched the active gait to Ripple 12 gait.";
const char string_8[] PROGMEM = "Rotate mode selected.";
const char string_9[] PROGMEM = "Single leg mode selected.";
const char string_10[] PROGMEM = "I am going to __sit down now.";
const char string_11[] PROGMEM = "I am trying to _stand up now.";
const char string_12[] PROGMEM = "Translate mode selected.";
const char string_13[] PROGMEM = "Switched the active gait to Tripod 6 gait.";
const char string_14[] PROGMEM = "Switched the active gait to Tripod 8 gait.";
const char string_15[] PROGMEM = "Switched the active gait to Tripple tripod 12 gait.";
const char string_16[] PROGMEM = "Switched the active gait to Tripple tripod 16 gait.";
const char string_17[] PROGMEM = "Walking mode selected.";
const char string_18[] PROGMEM = "Switched the active gait to Wave 24 gait.";
const char string_19[] PROGMEM = "I __got a connection with the commander.";
const char string_20[] PROGMEM = "I __lost the connection to the commander.";
const char string_21[] PROGMEM = "Balance mode is disabled.";
const char string_22[] PROGMEM = "Balance mode is enabled.";
const char string_23[] PROGMEM = "Selected other leg to control.";
const char string_24[] PROGMEM = "Debugging is enabled.";
const char string_25[] PROGMEM = "Debugging is disabled.";
const char string_26[] PROGMEM = "I wil __try to hold the selected leg in current position. Don't get your hopes up, hahaha.";
const char string_27[] PROGMEM = "The leg is released.";
const char string_28[] PROGMEM = "The lights are turned __on.";
const char string_29[] PROGMEM = "The lights are turned __off.";
const char string_30[] PROGMEM = "Double travel __and double height are now active.";

const char* const string_table[] PROGMEM = { string_0 , string_1 , string_2 , string_3 , string_4 , string_5 , string_6 , string_7 , string_8 , string_9 , string_10, string_11, string_12, string_13, string_14, string_15, string_16, string_17, string_18, string_19, string_20, string_21, string_22, string_23, string_24, string_25, string_26, string_27, string_28, string_29, string_30 };

char buffer[200];

String getFromStringTable(byte idx) {
	if (idx > sizeof(string_table)) {
		return "";
	}
	else
	{
		strcpy_P(buffer, (char*)pgm_read_word(&(string_table[idx])));
		return String(buffer);
	}
}