/* =============================================================================
/ Project N.I.K.K.I.E.
/ Description: Phoenix HexaPod code, converted to PhantomX by
/              Xan, KurtE and Zenta and rebuild for Teensy 4.1 by CMBSolutions
/ Software version: 1.0
/ Date: 10-06-2020
/ Programmer: CMBSolutions
/
/=============================================================================
/ Header Files
/=============================================================================
*/

#define HEXMODE   // default to hex mode
#define DISPLAY_GAIT_NAMES
#define DEFINE_HEX_GLOBALS

#include <Arduino.h>
#include <EEPROM.h>
//#include <avr/pgmspace.h>

#include <elapsedMillis.h>
#include <XBee.h>

#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <utility/Sd2Card.h>
#include <SerialFlash.h>

#include <Adafruit_BNO055.h>
#include <Adafruit_Sensor.h>
#include <utility/imumaths.h>

#include "stringTable.h"
#include "config.h"

#include "ax12serial.h"
#include "PhantomX_Core.h"

#include "PhantomX_InputCommander.h"
#include "PhantomX_AX12Driver.h"
#include "PhantomX_Code.h"
